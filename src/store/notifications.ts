import { inject, provide as provideVue, reactive, readonly } from "vue";

interface Notification {
  title?: string;
  body: string;
  type: string;
  stamp: Date;
}

interface NotificationObject {
  notifications: Notification[];
}

const state = reactive<NotificationObject>({
  notifications: [],
});

function provide() {
  provideVue("notifications", state);
}

function use() {
  const store = inject("notifications");
  return store;
}

function doNotification(body: string, type: string, title?: string): void {
  /* Create the notification */
  const notification: Notification = {
    title: title,
    body: body,
    type: type,
    stamp: new Date(),
  };
  /* Push the notification on the stack */
  state.notifications.push(notification);
}

const success = function(body: string, title?: string): void {
  doNotification(body, "success", title);
};

const error = function(body: string, title?: string): void {
  doNotification(body, "danger", title);
};

const warning = function(body: string, title?: string): void {
  doNotification(body, "warning", title);
};

const clearNotification = function(): void {
  state.notifications.pop();
};

export default {
  state: readonly(state),
  provide,
  use,
  success,
  error,
  warning,
  clearNotification,
};
