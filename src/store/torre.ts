import { IFilter } from "./../utils/Filters";
import { inject, provide as provideVue, reactive, readonly } from "vue";
import axios, { AxiosResponse } from "axios";

let cancelFn: any;

interface TorreObject {
  loading: boolean;
  type: number;
  queryString: string;
  searchResults: any[];
  offset: number;
  size: number;
  total: number;
  aggregators: any;
  filters: any[];
}

const state = reactive<TorreObject>({
  loading: false,
  type: 0,
  queryString: "",
  searchResults: [],
  offset: 0,
  size: 50,
  total: 0,
  aggregators: {},
  filters: [],
});

function provide() {
  provideVue("torre", state);
}

function use() {
  const store = inject("torre");
  return store;
}

const fetchPeople = function(
  name: string,
  offset = 0,
  size = 50
): Promise<any> {
  return new Promise<any>((resolve, reject) => {
    /* Check if the request must eb cancelled */
    if (cancelFn) {
      cancelFn();
    }

    state.loading = true;

    /* Add main query */
    const andQuery: any[] = [];
    andQuery.push({ name: { term: name } });

    /* Set the registered filters */
    state.filters.forEach((filter: any) => {
      andQuery.push(filter);
    });

    axios
      .post(
        `https://search.torre.co/people/_search/`,
        {
          and: andQuery,
        },
        {
          params: {
            aggregate: true,
            offset: offset,
            size: size,
          },
          headers: {
            "Content-type": "application/json",
          },
          cancelToken: new axios.CancelToken((c) => {
            cancelFn = c;
          }),
        }
      )
      .then((res: AxiosResponse) => {
        state.type = 0;
        state.queryString = name;
        state.searchResults = res.data.results;
        state.offset = res.data.offset;
        state.total = res.data.total;
        state.aggregators = res.data.aggregators;
        resolve(res.data);
      })
      .catch(reject)
      .finally(() => {
        cancelFn = null;
        state.loading = false;
      });
  });
};

const fetchJobs = function(skill: string, offset = 0, size = 50): Promise<any> {
  return new Promise<any>((resolve, reject) => {
    /* Check if the request must eb cancelled */
    if (cancelFn) {
      cancelFn();
    }

    state.loading = true;

    /* Add main query */
    const andQuery: any[] = [];
    andQuery.push({
      "skill/role": {
        experience: "potential-to-develop",
        text: skill,
      },
    });

    /* Set the registered filters */
    state.filters.forEach((filter: any) => {
      andQuery.push(filter);
    });

    axios
      .post(
        `https://search.torre.co/opportunities/_search/`,
        {
          and: andQuery,
        },
        {
          params: {
            aggregate: true,
            offset: offset,
            size: size,
          },
          headers: {
            "Content-type": "application/json",
          },
          cancelToken: new axios.CancelToken((c) => {
            cancelFn = c;
          }),
        }
      )
      .then((res: AxiosResponse) => {
        state.type = 1;
        state.queryString = skill;
        state.searchResults = res.data.results;
        state.offset = res.data.offset;
        state.total = res.data.total;
        state.aggregators = res.data.aggregators;
        resolve(res.data);
      })
      .catch(reject)
      .finally(() => {
        cancelFn = null;
        state.loading = false;
      });
  });
};

const loadPage = function(page: number): Promise<any> {
  if (state.type === 1) {
    return fetchJobs(state.queryString, (page - 1) * state.size, state.size);
  }
  return fetchPeople(state.queryString, (page - 1) * state.size, state.size);
};

const applyFilters = function(filter: IFilter): Promise<any> {
  state.filters = [];

  if (state.type === 0) {
    /* Check opento filter */
    if (filter.type && filter.type.length > 0) {
      state.filters.push({
        or: filter.type.map((value: any) => {
          return { opento: { term: value } };
        }),
      });
    }

    /* Check skills filter */
    if (filter.skills && filter.skills.length > 0) {
      state.filters.push({
        or: filter.skills.map((value: any) => {
          return {
            "skill/role": { text: value, experience: "potential-to-develop" },
          };
        }),
      });
    }
    return fetchPeople(state.queryString, state.offset, state.size);
  } else {
    /* Check job status filter */
    if (filter.status) {
      state.filters.push({ status: { code: filter.status } });
    }
    /* Check job type filter */
    if (filter.type && filter.type.length > 0) {
      state.filters.push({
        or: filter.type.map((value: any) => {
          return { type: { code: value } };
        }),
      });
    }

    /* Check organization filter */
    if (filter.organization && filter.organization.length > 0) {
      state.filters.push({
        or: filter.organization.map((value: any) => {
          return {
            organization: {
              term: value,
            },
          };
        }),
      });
    }

    /* Check location filter */
    if (filter.location && filter.location.length > 0) {
      state.filters.push({
        or: filter.location.map((value: any) => {
          return {
            location: {
              term: value,
            },
          };
        }),
      });
    }

    return fetchJobs(state.queryString, state.offset, state.size);
  }
};

const fetchUser = function(name: string): Promise<any> {
  return new Promise<any>((resolve, reject) => {
    /* Check if the request must eb cancelled */
    if (cancelFn) {
      cancelFn();
    }

    state.loading = true;

    axios
      .get(`http://127.0.0.1:3000/v1/user/${name}`, {
        cancelToken: new axios.CancelToken((c) => {
          cancelFn = c;
        }),
      })
      .then((res: AxiosResponse) => {
        resolve(res.data);
      })
      .catch(reject)
      .finally(() => {
        cancelFn = null;
        state.loading = false;
      });
  });
};

const fetchJob = function(name: string): Promise<any> {
  return new Promise<any>((resolve, reject) => {
    /* Check if the request must eb cancelled */
    if (cancelFn) {
      cancelFn();
    }

    state.loading = true;

    axios
      .get(`https://torre.co/api/opportunities/${name}`, {
        headers: {
          "Content-type": "application/json",
        },
        cancelToken: new axios.CancelToken((c) => {
          cancelFn = c;
        }),
      })
      .then((res: AxiosResponse) => {
        resolve(res.data);
      })
      .catch(reject)
      .finally(() => {
        cancelFn = null;
        state.loading = false;
      });
  });
};

export default {
  state: readonly(state),
  provide,
  use,
  fetchPeople,
  fetchJobs,
  loadPage,
  applyFilters,
  fetchUser,
  fetchJob,
};
