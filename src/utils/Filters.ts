export interface IFilter {
  status?: string;
  type?: string[];
  organization?: string[];
  location?: string[];
  skills?: string[];
}
