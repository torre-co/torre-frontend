import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "",
    component: () => import("@/layouts/MainFrontend.vue"),
    meta: {
      requireAuth: false,
    },
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("@/views/Home.vue"),
        meta: {
          requireAuth: false,
        },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
