import "@/styles/settings/_colors.scss";
import "@/plugins/bootstrap";

import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";

createApp(App)
  .use(router)
  .mount("#app");
